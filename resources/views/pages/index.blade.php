@extends('layouts.main')

@section('title', 'Diversidade nas Empresas')

@section('content')

<div class="cookies-container">
    <div class="cookies-content">
        <p>Usamos cookies em nosso site para fornecer a experiência mais relevante, lembrando suas preferências 
            e visitas repetidas. Ao clicar em “Aceitar todos”, você concorda com o uso de TODOS os cookies. 
            No entanto, você pode visitar <a href="" style="text-decoration: none" data-bs-toggle="modal" data-bs-target="#modalCookies">"Configurações de cookies"</a> para fornecer um consentimento controlado.</p>                
        <div class="container-fluid">
            <div class="row">
                <div class="col no-padding">
                    <button class="btn btn-success cookies-save mt-3 d-block mx-auto">Aceitar todos</button>            
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalCookies" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <img src="/img/logos/logo_ceert.png" class="img-fluid me-auto">
                <h5 class="modal-title mx-auto" id="exampleModalLabel">Centro de preferências de privacidade</h5>
                <button type="button" class="btn-close ms-auto" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="cookies-pref">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4 no-padding">
                                <ul class="menu-modal">
                                    <li class="active" id="#modal-necessarios">
                                        <a>Cookies estritamente necessários</a>
                                    </li>

                                    {{--<li id="#modal-desempenho">
                                        <a>Cookies de desempenho</a>
                                    </li>

                                    <li id="#modal-funcionalidade">
                                        <a>Cookies de funcionalidade</a>
                                    </li>

                                    <li id="#modal-publicidade">
                                        <a>Cookies de publicidade</a>
                                    </li>--}}
                                </ul>
                            </div>
                            <div class="col-md-8 p-3">
                                <div class="content" id="modal-necessarios">
                                    <label class="form-check-label"><input class="form-check-input" type="checkbox" checked data-function="cookiesNecessarios"> Cookies necessários</label>
                                    <p>São cookies essenciais que possibilitam a navegação em nossas aplicações e o acesso a todos os recursos; sem estes, nossos serviços podem apresentar mal desempenho ou não funcionar.</p>
                                </div>

                                {{--<div class="content" id="modal-desempenho" style="display: none">
                                    <label class="form-check-label"><input class="form-check-input" type="checkbox" checked data-function="cookiesDesempenho"> Cookies de desempenho</label>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec semper metus nec velit ultricies, id varius tortor eleifend. Aenean placerat gravida nisi, quis congue urna vulputate sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu varius enim. Curabitur nec ornare urna. Donec diam enim, commodo id scelerisque quis, porta in libero. Nulla facilisi. Donec varius molestie orci, in sagittis mi volutpat a. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin eleifend ornare pellentesque. Sed et pharetra purus, non varius eros. Donec lobortis ipsum sapien, ut mollis mi sagittis ac.</p>
                                </div>

                                <div class="content" id="modal-funcionalidade" style="display: none">
                                    <label class="form-check-label"><input class="form-check-input" type="checkbox" checked data-function="cookiesFuncionalidade"> Cookies de funcionalidade</label>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec semper metus nec velit ultricies, id varius tortor eleifend. Aenean placerat gravida nisi, quis congue urna vulputate sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu varius enim. Curabitur nec ornare urna. Donec diam enim, commodo id scelerisque quis, porta in libero. Nulla facilisi. Donec varius molestie orci, in sagittis mi volutpat a. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin eleifend ornare pellentesque. Sed et pharetra purus, non varius eros. Donec lobortis ipsum sapien, ut mollis mi sagittis ac.</p>
                                </div>

                                <div class="content" id="modal-publicidade" style="display: none">
                                    <label class="form-check-label"><input class="form-check-input" type="checkbox" checked data-function="cookiesPublicidade"> Cookies de publicidade</label>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec semper metus nec velit ultricies, id varius tortor eleifend. Aenean placerat gravida nisi, quis congue urna vulputate sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu varius enim. Curabitur nec ornare urna. Donec diam enim, commodo id scelerisque quis, porta in libero. Nulla facilisi. Donec varius molestie orci, in sagittis mi volutpat a. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin eleifend ornare pellentesque. Sed et pharetra purus, non varius eros. Donec lobortis ipsum sapien, ut mollis mi sagittis ac.</p>
                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer mt-5">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary cookies-save-modal" style="background-color: #198754; border-color: #198754">Salvar configurações</button>
            </div>
        </div>
    </div>
</div>
    
<!-- INÍCIO DO CAROUSEL -->
<div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">              
            <div class="bloco-carousel">
                <img src="/img/header-fundo.png" class="d-block w-100">

                <div class="degrade"></div>
                
                <div class="txt">
                    <h3>O DADO COR/RAÇA NOS CADASTROS:</h3>
                    <h1>DIAGNÓSTICO E MONITORAMENTO DAS DESIGUALDADES RACIAIS</h1>
                </div>
            </div>                
        </div> 

        <div class="carousel-item">              
            <div class="bloco-carousel">
                <img src="/img/header-fundo.png" class="d-block w-100">

                <div class="degrade"></div>
                
                <div class="txt">
                    <h3>O DADO COR/RAÇA NOS CADASTROS:</h3>
                    <h1>DIAGNÓSTICO E MONITORAMENTO DAS DESIGUALDADES RACIAIS</h1>
                </div>
            </div>                
        </div> 

        <div class="carousel-item">              
            <div class="bloco-carousel">
                <img src="/img/header-fundo.png" class="d-block w-100">

                <div class="degrade"></div>
                
                <div class="txt">
                    <h3>O DADO COR/RAÇA NOS CADASTROS:</h3>
                    <h1>DIAGNÓSTICO E MONITORAMENTO DAS DESIGUALDADES RACIAIS</h1>
                </div>
            </div>                
        </div> 
    </div>   
    
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>
<!-- FIM DO CAROUSEL -->    

<!-- INÍCIO SECTION AUTOR -->
<section id="autor">      
    <div class="container" data-aos="fade-up">
        <div class="row">
            <div class="col-1">
                <div class="linha-autor"></div>
            </div>

            <div class="col-11">
                <div class="img-nomeMario">
                    <img class="img-fluid" src="img/foto-autor.png" alt="Foto Autor">
                    <br>
                    <br>                  
                    <h5>
                        <p>POR</p>
                        MÁRIO ROGÉRIO SILVA
                    </h5>
                </div>

                <div class="txt-Mário">
                    <h3>A geração de informações sobre a presença ou a ausência de afrodescendentes nas mais diversas instituições do País é essencial para embasar ações direcionadas a superar a histórica marginalização desse segmento da população brasileira. A escassez de dados organizados nesse âmbito é um fato conhecido por quem se preocupa com a resolução da questão, uma vez que dificulta a criação de políticas públicas ou privadas voltadas para a promoção da equidade e a diminuição do preconceito relacionado a raça e etnia.</h3>
                    <br>
                    <h4>A cartilha Orientações para a Qualificação do Quesito Cor ou Raça no Censo da Educação Superior, disponível no site da Secretaria Especial <span id="pontos"></span> <span id="mais"> de Políticas de Promoção da Igualdade Racial do Ministério das Mulheres, da Igualdade Racial e dos Direitos Humanos, fala sobre a importância desse quesito, que nada mais é do que um conjunto de perguntas sobre o tema dentro dos questionários nos quais são coletados dados pessoais para inclusão em cadastros de instituições: “A coleta de dados sobre “cor ou raça” da população, a análise e difusão de informações a partir desses dados – diz o texto da Seppir – têm reiteradamente comprovado a existência de significativas diferenças socioeconômicas e culturais entre a população negra e a não negra. Essas diferenças revelam persistentes desigualdades raciais que precisam ser cada vez melhor dimensionadas para que possamos mitigá-las”. A questão da coleta de dados sobre raça e cor esteve no centro dos debates que aconteceram no decorrer do período imediatamente anterior à histórica decisão do Supremo Tribunal Federal (STF) que validou por unanimidade a adoção de políticas de reserva de vagas para garantir o acesso de negros e índios a instituições de ensino superior em todo o país. Os adversários das cotas diziam (e dizem) que a simples coleta dessa informação iria agravar o racismo e apontavam a impossibilidade e o perigo da classificação racial que, segundo eles, estaria sendo implantada no País agora, por conta das políticas de ação afirmativa. A verdade, como mostraremos mais à frente, é que a captação e o uso desse tipo de informação têm um histórico antigo no país, e sua expansão neste momento atende à necessidade de atuar concretamente no sentido da promoção da equidade entre os diversos segmentos étnico-raciais. No momento em que este artigo estava sendo escrito, circulava pelas redes sociais a CARTA-MANIFESTO: Não Aceitaremos Brancos em Nossas Vagas!, documento no qual as principais entidades do movimento negro brasileiro denunciavam e pediam providências para evitar que candidatos brancos que fraudaram a autodeclaração e se disseram negros ou pardos ocupassem parte das 111 vagas destinadas a negros no concurso público para o provimento de 556 cargos do quadro de pessoal do Ministério do Planejamento, Orçamento e Gestão (MPOG) e da Escola Nacional de Administração Pública (ENAP). Lembrando a grande quantidade de concursos públicos e as oportunidades que significam para milhares de afrodescendentes, esse episódio torna-se um exemplo da importância vital que tem a coleta do dado cor/raça para a implementação de políticas de promoção da igualdade, colocando na ordem do dia a necessidade de aprimoramento da metodologia de coleta desse tipo de informação. A perspectiva de transversalidade racial e de gênero nas políticas públicas, e consequentemente na empregabilidade, implica adotar metodologia que observe as situações, a partir de dados estatísticos, que não deixam margem a argumentos sobre as desigualdades entre os diversos grupos raciais. Nesse contexto, a introdução do quesito cor/raça nos cadastros públicos e privados se torna essencial. É uma forma ímpar de combater os estigmas, pois permite evidenciar que o problema não é dos grupos discriminados, mas da sociedade discriminadora. A ausência desse dado confere uma neutralidade aos diagnósticos, como se mulheres e negros experimentassem as condições de trabalho, saúde e educação de maneira similar. E não é essa realidade que as investigações realizadas pelos principais órgãos de pesquisas brasileiros revelam quando focalizam as relações raciais. Depois de demonstrar o vínculo direto entre a dificuldade de obtenção dessa informação e a ideologia da democracia racial, o texto faz um breve histórico da luta do Movimento Negro pela inclusão do quesito cor/raça nos cadastros públicos e privados, concluindo com a apresentação de uma experiência bem-sucedida de superação de resistências e aperfeiçoamento na coleta desse tipo de informação, realizada pelo Ceert, em 2014 no projeto SINE – Sistema Nacional de Emprego – operado pela Secretaria de Trabalho, Emprego, Renda e Esporte da Bahia.</span></h4>

                    <div class="btn-autor">                        
                        <button class="btn" id="btnLerMais">LER MAIS</button>
                    </div>
                </div>
            </div>                   
        </div>
    </div>
</section>

<!-- INÍCIO DA SECTION RACISMO -->  
<section id="racismo">  
    <div class="container" data-aos="fade-up">
        <div class="row">
            <div class="col-1" id="col-linhatxt">                    
                <div class="linha-racismo"></div>                     
            </div>          

            <div class="col-11">     
                <div class="row">
                    <div class="col-12">
                        <div class="txt-lateral">
                            <p id="txt-racismolateral"><span id="invisibilidade">INVISIBILIDADE DO </span>RACISMO</p>
                        </div>  
                    </div>

                    <div class="col-12">
                        <div class="img-descrição01-centralizar">
                            <img class="img-fluid" id="foto-descrição" src="img/foto-descricao.png" alt="foto-descrição">
                        </div>

                        <div class="txt-descrição1">
                            <h4>Apesar dos avanços nos últimos anos na luta contra o racismo, a ausência da informação sobre cor/raça nos cadastros públicos e 
                                privados tem sido um grande obstáculo na compreensão e no enfrentamento do racismo, projetando análises e diagnósticos equivocados, 
                                como se todos tivessem oportunidades iguais. 
                                <br><br>
                                Estudar e analisar em profundidade características e desafios e propor mudanças com a introdução da informação sobre cor requerem o 
                                esforço para que a coleta do quesito cor seja feita com qualidade e em todos os cadastros públicos e privados.
                            </h4>
                        </div>  
                    </div>

                    <div class="col-12">
                        <div class="caixa-pontilhada1">
                            <span id="aspas-iníciotxt1">"</span>                            

                            <p>A GERAÇÃO DE INFORMAÇÕES SOBRE A PRESENÇA OU AUSÊNCIA DE AFRODESCENDENTES NAS MAIS DIVERSAS INSTITUIÇÕES DO PAÍS É ESSENCIAL PARA EMBASAR AÇÕES DIRECIONADAS A SUPERAR A HISTÓRICA MARGINALIZAÇÃO DESSE SEGMENTO DA POPULAÇÃO BRASILEIRA.</p>                            

                            <span id="aspas-finaltxt1">"</span>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="img-descrição02-centralizar">
                            <img class="img-fluid" src="img/foto-descricao-2.png" alt="Foto-descrição2" id="foto-descrição2">
                        </div>  

                        <div class="txt-descrição2">
                            <h4>Este mundo sem estranhos, que não tolera o diferente e o novo, é o mundo das ideologias totalitárias, conhecidas pela tendência de localizar o misterioso, aquele que não se define, e buscar a destruição do que não consegue controlar. 
                                <br><br>
                                No entanto, atualmente, quando se fala da necessidade de respeitar o diferente e valorizar a diversidade, lidar com o estranho torna necessária outra lógica, que por sua vez pode gerar um tipo de respeito e convivência com a alteridade que, de certa forma, acentua o racismo. 
                                <br><br>
                                E o desconforto frente à necessidade de conviver com a alteridade emerge. O problema não é como se livrar do estranho e do diferente de uma vez por todas, mas conviver com a alteridade diária e permanentemente. 
                                <br><br>
                                Como os outros espaços da vida social, no Brasil, o mundo do trabalho é um locus onde podemos observar as principais questões que envolvem a invisibilização, estigmatização e exclusão. Aqui a relação social está historicamente marcada pela desigualdade e pela discriminação racial.</h4>
                        </div>  
                    </div>

                    <div class="col-12">
                        <div class="caixa-pontilhada2">
                            <span id="aspas-iníciotxt2">"</span>

                            <p>A AUSÊNCIA DA INFORMAÇÃO SOBRE RAÇA/COR NOS CADASTROS PÚBLICOS E PRIVADOS TEM SIDO UM GRANDE OBSTÁCULO NA COMPREENSÃO E ENFRENTAMENTO DO RACISMO</p>
                            
                            <span id="aspas-finaltxt2">"</span>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <div class="txt-final">
                            <h4>A invisibilização passa a ser uma estratégia para o modelo brasileiro de relações raciais. De um lado, Oracy Nogueira afirma que a miscigenação funde as origens, mas mantém intactas as marcas; e, de outro, Gilberto Freyre diz que a fusão das origens elimina as marcas, criando as condições para o surgimento de uma ideia cara ao país: a negação da problemática racial. Esta negação será expressa pelo discurso das elites na historiografia, nas ciências sociais e na identidade dos descendentes de africanos. Passa a prevalecer a noção de que o não falar, não ver, não visibilizar, seria a solução para a estrutura racializada que marca a história brasileira.
                                <span id="quebra"></span><span id="more"> Trabalhos das últimas duas décadas vêm revelando que, com a evolução da sociedade de classes, as desigualdades não só se mantiveram como em alguns casos recrudesceram, a exemplo do Sudeste. Desmancham-se velhos credos que atribuíam as desigualdades raciais atuais apenas a um difuso legado do passado escravista e ligavam sua pretensa superação às transformações do sistema capitalista. É factual que o sistema capitalista é um dos principais mediadores do racismo, criando e recriando persistentemente condições propícias à sua reprodução. O que não significa que se possa reduzir tudo a uma questão de classes. Certo é que, seja em função da tentativa de se negar cabalmente o racismo, seja pela tentativa de subordiná-lo ao debate sobre classes, a invisibilidade acompanha a trajetória do racismo brasileiro.
                                </span>
                            </h4> 

                            <div class="btn-racismo">                            
                                <button class="btn" id="btnContinuação">LER MAIS</button>
                            </div>
                        </div>  
                    </div>
                </div>   
            </div>
        </div>
    </div>
</section>

<section id="cor">
    <div class="img-fundo"></div>                  
    <div class="bloco-fundo"></div>

    <div class="container" data-aos="fade-up">
        <div class="row">
            <div class="col-md-1"></div>

            <div class="col-md-11">
                <div class="row">
                    <div class="col-12">
                        <p id="título">CLASSIFICAÇÃO RACIAL E QUESITO COR</p>
                    </div>          

                    <div class="col-12">
                        <div class="img-descrição03-centralizar">
                            <img class="img-fluid" src="img/foto-descricao-3.png" alt="Foto Cor" id="foto-descrição3">
                        </div>

                        <p id="txt-col1">Ao contrário do que postula o senso comum, a classificação racial de pessoas está há muito tempo presente nos recenseamentos, mas também em alguns importantes cadastros públicos. Nos argumentos contra a classificação racial, o que se revela é a preocupação com uma suposta dificuldade em fazê-la, devido à miscigenação. Esse argumento trata a classificação racial como se fosse uma prática inédita que, teoricamente, teria surgido com a adoção de cotas nas universidades, ignorando que ela existe oficialmente há mais de 140 anos.</p>

                        <p id="txt-col2">Segundo Kabengele Munanga, os conceitos e as classificações servem de ferramentas para operacionalizar o pensamento. Em qualquer operação de classificação, é preciso estabelecer critérios objetivos com base nas diferenças e nas semelhanças5. Munanga afirma que os naturalistas dos séculos XVIII e XIX não limitaram seus trabalhos somente à classificação dos grupos humanos em função das características físicas, mas se deram o direito de hierarquizar, isto é, estabelecer uma escala de valores entre as chamadas raças.</p>
                    </div>

                    <div class="col-12">
                        <div class="caixa-pontilhada3">  
                            <span id="aspas-iníciotxt3">"</span>
                            
                            <p>(...) A INTRODUÇÃO DO QUESITO COR NOS CADASTROS PÚBLICOS E PRIVADOS SE TORNA ESSENCIAL. É UMA FORMA ÍMPAR DE COMBATER OS ESTIGMAS (...)</p>            

                            <span id="aspas-finaltxt3">"</span>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <p id="txt-finalcor">A rigor, trata-se de um fenômeno que ganhou relevância a partir de 1995, quando as principais entidades e lideranças do 
                            Movimento Negro entregaram um documento ao Presidente da República com um diagnóstico pormenorizado da situação dos negros no Brasil e 
                            passaram a reivindicar abertamente políticas de promoção da igualdade racial. 
                            <br><br>Em novembro de 1995, os principais jornais do país registravam a mais notável manifestação contemporânea de rua organizada pelo 
                            Movimento Negro Brasileiro: a Marcha Zumbi dos Palmares Contra o Racismo, pela Cidadania e pela Vida, que reuniu cerca de 30 mil pessoas 
                            em Brasília no dia 20 de novembro daquele ano, ocasião em que os coordenadores do evento se encontraram com o presidente Fernando Henrique 
                            Cardoso e a ele entregaram um documento pactuado entre as principais organizações e lideranças negras do país. 
                            <span id="quebratxtfinal"></span> <span id="maistxt"> Nesse documento pode-se ler: “Democratização da Informação – inclusão do quesito cor em 
                            todo e qualquer sistema de informação sobre a população, cadastro do funcionalismo, usuários de serviços internos em instituições públicas, 
                            empregados, desempregados, inativos e pensionistas, e, particularmente, nas declarações de nascimentos, prontuários e atestado de óbito 
                            (grifo meu) para que se conheça o perfil da morbidade e da mortalidade da população negra no país. A criação desta base de dados da população 
                            negra é fundamental para a formulação de políticas públicas específicas para todas as áreas de interesse da questão racial”8. Fato é que a 
                            Marcha representou não apenas um promissor momento de ação unificada do conjunto da militância, como também marcou a eleição da proposta de 
                            políticas de promoção da igualdade como um tema de consenso no discurso da liderança negra. Algumas ações resultaram desses processos, como o 
                            Decreto 1.904, de 13 de maio de 1996, que instituiu o Programa Nacional de Direitos Humanos, com duas proposições ligadas à temática das 
                            políticas de promoção da igualdade: 1. Inclusão do quesito cor em todos e quaisquer sistemas de informação e registro sobre a população, e 
                            bancos de dados públicos; 2. Criação de um banco de dados sobre a situação dos direitos civis, econômicos e culturais da população negra na 
                            sociedade brasileira, que oriente políticas afirmativas para a promoção dessa comunidade9. A despeito de todo o debate registrado nos últimos 
                            anos, o ingresso das políticas de promoção da igualdade racial/cotas na mídia, nas casas legislativas e nos órgãos públicos ocorreu, 
                            inquestionavelmente, durante o processo preparatório da participação brasileira na Conferência de Durban, cujo auge aconteceu no segundo semestre 
                            de 2001.
                            </span>
                        </p>

                        <div class="btn-final">
                            <button class="btn" id="btnMaistxtfinal">LER MAIS</button>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>   
</section>

<!-- INÍCIO SECTION BAHIA -->
<section id="bahia">      
    <img class="img-fluid" src="img/sinebahia.png" alt="Sine Bahia">

    <div class="container" id="content-bahia" data-aos="fade-up">
        <div class="row">
            <div class="col-1" id="linha-txtbahia">
                <div class="linha-bahia"></div>      

                <!-- <div class="txt-lateralbahia">
                    <p><span id="projetosine">PROJETO SINE </span>BAHIA</p>
                </div>   -->
            </div>

            <div class="col-11">
                <div class="txt-lateralbahia">
                    <p><span id="projetosine">PROJETO SINE </span>BAHIA</p>
                </div>  

                <div class="txt-bahia">
                    <h4>Um exemplo de aperfeiçoamento de política pública a partir da utilização do quesito “cor” é a experiência do CEERT na implantação em 2002 de um sistema para coleta desta informação junto aos usuários da Central de Trabalho e Renda de Santo André (CRT/CUT), agência pública de empregos desse município da Grande São Paulo. 
                    <br><br>
                    No início do processo, havia 67,4% de respostas na alternativa “cor não declarada”. Vários fatores contribuíam para isso: o constrangimento dos atendentes ao fazer a pergunta sobre cor13 e, de outro lado, o incômodo dos usuários ao responder sobre sua cor. Era evidente a necessidade de capacitar profissionais para desenvolver esse processo e de informar a população respondente sobre os objetivos da coleta do dado cor, qual seja, considerar a situação da população negra na gestão das políticas públicas. Ao final de cinco meses de trabalho, apenas 3,5% de respostas correspondiam à categoria “cor não declarada”.</h4>
                </div>

                <div class="gráfico-bahia">
                    <img class="img-fluid" src="img/ctr-grafico.png" alt="Gráfico">
                </div>

                <div class="btn-sinelermais">
                    <button class="btn" id="btnsineLermais">LER MAIS</button>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- INÍCIO SECTION VER MAIS -->
<section id="ver-mais">    
    <div class="container" data-aos="fade-up">
        <div class="row row-cols-1 row-cols-md-3 align-items-center">
            <div class="col">
                <div class="imgtxt">
                    <a href="#">
                        <img class="img-fluid" src="img/imagem-pub-1.png">
                        <p id="legendas">capítulo anterior</p>
                    </a>
                </div>
            </div>

            <div class="col">
                <div class="imgtxt my-3 my-md-0">
                    <a href="#">
                        <img class="img-fluid" src="img/imagem-pub-2.png">
                        <p id="legendas">saiba mais</p>
                    </a>
                </div>
            </div>

            <div class="col">
                <div class="imgtxt">
                    <a href="#">
                        <img class="img-fluid" src="img/imagem-pub-3.png">
                        <p id="legendas">próximo capítulo</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="contato">
    <div class="container" data-aos="fade-up">
        <div class="row">
            <div class="col-md-4">
                <div class="redes">
                    <p id="redes-sociais">REDES SOCIAIS</p>
                    <li>
                        <a href="#">Site</a>
                    </li>
                    <li>
                        <a href="#">Instagram</a>
                    </li>
                    <li>
                        <a href="#">Facebook</a>
                    </li>
                    <li>
                        <a href="#">Linkedin</a>
                    </li>                    
                </div>
            </div>

            <div class="col-md-8 no-padding-md">
                <div class="forms">
                    <form method="POST" action="#">
                        <label for="email" id="txt-forms">Se inscreva para receber o ebook completo.</label>

                        <div class="row align-items-center">
                            <div class="form-group col-lg-9">                            
                                <input class="form-control" type="email" name="email" id="email" placeholder="mateuspaula@ceert.org.br">                             
                            </div>

                            <div class="form-group col-lg-3 my-3">
                                <button class="btn" type="submit" id="btn-forms">RECEBER</button>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>  

@endsection