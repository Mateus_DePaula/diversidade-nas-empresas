<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Fontes -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Raleway" rel="stylesheet">

        <!-- Css -->
        <link href="css/style.css" rel="stylesheet">

        <!-- Bootstrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">        
        
        <!-- Fade aos -->
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">        
    </head>

    <body>
        <header>
            <nav class="navbar navbar-expand-md">
                <div class="container-fluid">
                    <a class="navbar-brand" href="https://ceert.org.br/">
                        <img class="img-fluid" src="img/branco-st.png" alt="Logo CEERT">
                    </a>
                    
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarNav">                    
                        <ul class="navbar-nav ms-auto">
                            <li class="nav-item"><a class="nav-link active" aria-current="page" href="#">INÍCIO</a></li>
                            <li class="nav-item"><a class="nav-link" href="#autor">O AUTOR</a></li>
                            <li class="nav-item"><a class="nav-link" href="#racismo">INVISIBILIDADE DO RACISMO</a></li>
                            <li class="nav-item"><a class="nav-link" href="#cor">CLASSIFICAÇÃO RACIAL E QUESITO COR</a></li>
                            <li class="nav-item"><a class="nav-link" href="#bahia">PROJETO SINE BAHIA</a></li>
                            <li class="nav-item"><a class="nav-link" href="#contato">CONTATO</a></li>            
                        </ul>                    
                    </div>
                </div>
            </nav>
        </header>

        @yield('content')             

        <footer>            
            <p>&copy; 2021 CEERT. Todos os Direitos Reservados</p>
        </footer>

        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script src="/js/script.js"></script>        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>        
    </body>
</html>