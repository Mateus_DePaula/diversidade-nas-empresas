/* BOTÃO SECTION AUTOR */
$ ( "#btnLerMais" ).click(function() {
    $ ("#mais").slideToggle( "slow" ); 

    if ($(this).text() == 'LER MAIS') {
        $(this).text('LER MENOS');
    }else {
        $(this).text('LER MAIS');
    }    
});  
    


/* BOTÃO SECTION RACISMO */
$ ( "#btnContinuação" ).click(function() {
    $("#more").slideToggle( "slow" );

    if ($(this).text() == 'LER MAIS') {
        $(this).text('LER MENOS');
    }else {
        $(this).text('LER MAIS');
    }
});  



/* BOTÃO SECTION COR */
$ ( "#btnMaistxtfinal" ).click(function() {
    $("#maistxt").slideToggle( "slow" );

    if ($(this).text() == 'LER MAIS') {
        $(this).text('LER MENOS');
    }else {
        $(this).text('LER MAIS');
    }
});  


    
/* BOTÃO SECTION BAHIA */ 
$ ( "#btnsineLermais" ).click(function() {
    $("#").slideToggle( "slow" );
    
    if ($(this).text() == 'LER MAIS') {
        $(this).text('LER MENOS');
    }else {
        $(this).text('LER MAIS');
    }    
});  

$('header nav ul li a').click(function(){
    $('header nav ul li a').removeClass('active');
    $(this).addClass('active');
});



// Cookies
function cookies(functions) {
    const container = document.querySelector('.cookies-container');
    const save = document.querySelector('.cookies-save');
    const saveModal = document.querySelector('.cookies-save-modal');

    if (!container || !save || !saveModal) return null;

    const localPref = JSON.parse(window.localStorage.getItem('cookies-pref'));
    if (localPref) activateFunctions(localPref);

    function getFormPref() {
        return [...document.querySelectorAll('[data-function]')]
        .filter((el) => el.checked)
        .map((el) => el.getAttribute('data-function'));
    }

    function activateFunctions(pref) {
        pref.forEach((f) => functions[f]());
        container.style.display = 'none';
        window.localStorage.setItem('cookies-pref', JSON.stringify(pref));
    }

    function handleSave() {
        const pref = getFormPref();
        activateFunctions(pref);
        $('#modalCookies').modal('hide');
    }

    save.addEventListener('click', handleSave);
    saveModal.addEventListener('click', handleSave);
}

function cookiesNecessarios() {
    console.log('Função de cookiesNecessarios');
}

function cookiesDesempenho() {
    console.log('Função de cookiesDesempenho');
}

function cookiesFuncionalidade() {
    console.log('Função de cookiesFuncionalidade');
}

function cookiesPublicidade() {
    console.log('Função de cookiesPublicidade');
}

cookies({
    cookiesNecessarios,
    cookiesDesempenho,
    cookiesFuncionalidade,
    cookiesPublicidade
});

AOS.init();